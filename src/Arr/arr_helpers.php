<?php
/*
	arr_* helpers
	2017 by mhofer
	Feel free to use wherever you please
	Uses some code from Laravel 4
*/

if (!function_exists("arr_table"))
{
	function arr_table($array)
	{
		if (!sizeof($array))
		{
			return "";
		}

		$html = '<table class="table table-hover table-bordered table-striped table-condensed">';

		$keyitem = reset($array);
		if (is_object($keyitem))
		{
			$keyitem = (array) $keyitem;
		}

		if (is_string(arr_keys($keyitem)[0]))
		{
			$keys = [];

			foreach ($array as $idx => $value)
			{
				if (is_object($value))
				{
					$value = (array) $value;
				}

				foreach (arr_keys($value) as $key)
				{
					if (!isset($keys[$key]))
					{
						$keys[$key] = true;
					}
				}
			}

			$keys = arr_unique(arr_keys($keys));

			$html .= '<thead><tr>';
			foreach ($keys as $key)
			{
				$html .= '<th>' . $key . '</th>';
			}

			$html .= '</thead></tr>';
		}

		$html .= '<tbody>';

		foreach ($array as $idx => $value)
		{
			$html .= '<tr>';

			if (is_object($value))
			{
				$value = (array) $value;
			}

			foreach ($value as $key2 => $value2)
			{
				$html .= '<td>' . (is_array($value2) ? json_encode($value2) : $value2) . '</td>';
			}

			$html .= '</tr>';
		}

		$html .= '</tbody>';
		$html .= '</table>';

		return $html;
	}
}

if (!function_exists("arr_fromCSV"))
{
	function arr_fromCSV($filename, $unique = true, $sep = ",")
	{
		$ret = array_map(function($row) use ($sep) { return str_getcsv($row, $sep); }, file($filename));
		if ($unique) $ret = array_map("unserialize", array_unique(array_map("serialize", $ret)));

		return $ret;
	}
}

if (!function_exists("arr_fromCSV"))
{
	function arr_writeCSV($arr, $filename, $delim = ",")
	{
		if (file_exists($filename))
		{
			return false;
		}

		$fp = fopen($filename, 'w');
		foreach ($arr as $val)
		{
			fputcsv($fp, $val, $delim);
		}

		fclose($fp);
	}
}

if (!function_exists("arr_stdDev"))
{
	function arr_stdDev($aValues, $bSample = false)
	{
		if (!count($aValues)) return 0;
		
		$fMean = array_sum($aValues) / count($aValues);
		$fVariance = 0.0;
		foreach ($aValues as $i)
		{
		    $fVariance += pow($i - $fMean, 2);
		}

		$fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
		
		return (float) sqrt($fVariance);
	}
}

if (!function_exists("arr_isNumeric"))
{
	function arr_isNumeric($arr)
	{
		if (!is_array($arr)) return false;
		if (!sizeof($arr)) return true;

		return !is_string(array_keys($arr)[0]);
	}
}

if (!function_exists("arr_isAssoc"))
{
	function arr_isAssoc($arr)
	{
		if (!is_array($arr) || !sizeof($arr)) return false;
		return is_string(array_keys($arr)[0]);
	}
}

if (!function_exists("arr_count"))
{
	function arr_count($arr)
	{
		return sizeof($arr);
	}
}

if (!function_exists("arr_combine"))
{
	function arr_combine()
	{
		return call_user_func_array("array_combine", func_get_args());
	}
}

if (!function_exists("arr_sortBy"))
{
	function arr_sortBy($arr, $valRef, $dir = SORT_ASC)
	{
		$sort_col = [];

		if ($dir == "DESC")
		{
			$dir = SORT_DESC;
		}
		else
		{
			$dir = SORT_ASC;
		}

		if (is_object($valRef) && ($valRef instanceof Closure))
		{
			$func = function($row, $key, $valRef)
			{
				return $valRef($row, $key);
			};
		}
		else
		{
			$func = function($row, $key, $valRef)
			{
				return arr_get($row, $valRef);
			};
		}

		foreach ($arr as $key => $row)
		{
			$sort_col[$key] = $func($row, $key, $valRef);
		}

		array_multisort($sort_col, $dir, $arr);

		return $arr;
	}
}

if (!function_exists("arr_is"))
{
	function arr_is($arr)
	{
		return is_array($arr);
	}
}

if (!function_exists("arr_last"))
{
	function arr_last($arr)
	{
		return array_pop((arr_slice($arr, -1)));
	}
}

if (!function_exists("arr_some"))
{
	function arr_some($arr, $matcher)
	{
		return arr_findFirst($arr, $matcher);
	}
}

if (!function_exists("arr_findFirst"))
{
	function arr_findFirst($arr, $matcher)
	{
		$ret = arr_findWhere($arr, $matcher, true);

		return sizeof($ret) ? $ret[0] : null;
	}
}

if (!function_exists("arr_findLast"))
{
	function arr_findLast($arr, $matcher)
	{
		$ret = arr_findWhere(arr_reverse($arr), $matcher, true);

		return sizeof($ret) ? $ret[0] : null;
	}
}

if (!function_exists("arr_findWhere"))
{
	function arr_findWhere($arr, $matcher, $stopAtFirst = false)
	{
	    $ret = [];

		foreach ($arr as $key => $item)
		{
			if (is_callable($matcher))
			{
				$matches = false;
				if ($matcher($item, $key))
				{
					$matches = true;
				}
			}
			else if (is_string($matcher))
			{
				$matches = false;
				if (arr_get($item, $matcher))
				{
					$matches = true;
				}
			}
			else
			{
				$matches = true;
				foreach ($matcher as $attr => $match)
				{
					if (!isset($item[$attr]) || $item[$attr] !== $match)
					{
						$matches = false;
						break 1;
					}
				}
			}
			
			if ($matches)
			{
				$ret[] = $item;
				if ($stopAtFirst)
				{
					break 1;
				}
			}
		}

		return $ret;
	}
}

if (!function_exists("array_avg"))
{
	function array_avg($arr)
	{
		return !sizeof($arr) ? null : array_sum($arr) / sizeof($arr);
	}
}

if (!function_exists("arr_max"))
{
	function arr_max($arr)
	{
		return !sizeof($arr) ? null : max($arr);
	}
}

if (!function_exists("arr_min"))
{
	function arr_min($arr)
	{
		return !sizeof($arr) ? null : min($arr);
	}
}

if (!function_exists("arr_sum"))
{
	function arr_sum($arr)
	{
		return !sizeof($arr) ? 0 : array_sum($arr);
	}
}

if (!function_exists("arr_filter"))
{
	function arr_filter()
	{
		return call_user_func_array("array_filter", func_get_args());
	}
}

if (!function_exists("arr_filtervalues"))
{
	function arr_filtervalues()
	{
		return array_values(call_user_func_array("array_filter", func_get_args()));
	}
}

if (!function_exists("arr_unique"))
{
	function arr_unique($array, $sort_flags = null)
	{
		if (!$sort_flags && sizeof($array) && arr_in(["object", "array"], gettype(reset($array))))
		{
			$serialized = array_map('serialize', $array);
			$unique = array_unique($serialized);
			return array_intersect_key($array, $unique);
		}

		return call_user_func_array("array_unique", func_get_args());
	}
}

if (!function_exists("arr_uniquevalues"))
{
	function arr_uniquevalues($array, $sort_flags = null)
	{
		return array_values(arr_unique($array, $sort_flags));
	}
}

if (!function_exists("arr_map"))
{
	function arr_map($arr, $callback)
	{
		if (is_string($callback))
		{
			return array_map($callback, $arr);
		}
		
		$ret = [];
		foreach ($arr as $idx => $item)
		{
			$ret[] = $callback($item, $idx);
		}

		return $ret;
	}
}

if (!function_exists("arr_merge"))
{
	function arr_merge()
	{
		return call_user_func_array("array_merge", func_get_args());
	}
}

if (!function_exists("arr_values"))
{
	function arr_values($arr)
	{
		return array_values($arr);
	}
}

if (!function_exists("arr_keys"))
{
	function arr_keys($arr)
	{
		return array_keys($arr);
	}
}

if (!function_exists("arr_slice"))
{
	function arr_slice($arr)
	{
		return call_user_func_array('array_slice', func_get_args());
	}
}

if (!function_exists("arr_reverse"))
{
	function arr_reverse()
	{
		return call_user_func_array('array_reverse', func_get_args());
	}
}

if (!function_exists("arr_percentile"))
{
	function arr_percentile($array, $percentile)
	{
		$array = arr_values($array);

		sort($array);
		
		$index = ($percentile / 100) * count($array);
		
		if (floor($index) == $index)
		{
			$result = ($array[$index-1] + $array[$index])/2;
		}
		else
		{
			$result = $array[floor($index)];
		}

		return $result;
	}
}

if (!function_exists("arr_scale"))
{
	function arr_scale($arr, $min = 0, $max = 1)
	{
		$max = arr_max($arr);

		if (!sizeof($arr) || $max == 0) return $arr;

		$ret = [];
		foreach ($arr as $val)
		{
			$ret[] = $val / $max;
		}

		return $ret;
	}
}

if (!function_exists("arr_merge_recursive"))
{
	function arr_merge_recursive()
	{
		if (func_num_args() < 2)
		{
			trigger_error(__FUNCTION__ .' needs two or more array arguments', E_USER_WARNING);
			return;
		}

		$arrays = func_get_args();
		$merged = [];

		while ($arrays)
		{
			$array = array_shift($arrays);

			if (!is_array($array)) continue;

			foreach ($array as $key => $value)
			{
				if (is_string($key))
				{
					if (is_array($value) && array_key_exists($key, $merged) && is_array($merged[$key]))
					{
						$merged[$key] = call_user_func(__FUNCTION__, $merged[$key], $value);
					}
					else
					{
						$merged[$key] = $value;
					}
				}
				else
				{
					$merged[] = $value;
				}
			}
		}

		return $merged;
	}
}

if (!function_exists("arr_groupBy"))
{
	function arr_groupBy($arr, $key)
	{
		$ret = [];
		foreach ($arr as $item)
		{
			$ret[$item[$key]][] = $item;
		}

		return $ret;
	}
}

if (!function_exists("arr_diff"))
{
	function arr_diff()
	{
		return call_user_func_array('array_diff', func_get_args());
	}
}

if (!function_exists("arr_in"))
{
	function arr_in($haystack, $needle, $strict = false)
	{
		return in_array($needle, $haystack, $strict);
	}
}

if (!function_exists("arr_intersect"))
{
	function arr_intersect()
	{
		return call_user_func_array('array_intersect', func_get_args());
	}
}

if (!function_exists("arr_key_exists"))
{
	function arr_key_exists($arr, $key)
	{
		return array_key_exists($key, $arr);
	}
}

if (!function_exists("arr_keymap"))
{
	function arr_keymap($arr, $key)
	{
		return array_combine(arr_pluck($arr, $key), arr_values($arr));
	}
}

if (!function_exists("arr_chunk"))
{
	function arr_chunk()
	{
		return call_user_func_array('array_chunk', func_get_args());
	}
}

if (!function_exists("arr_add"))
{
	function arr_add($array, $key, $value)
	{
		if (is_null(arr_get($array, $key)))
		{
			arr_set($array, $key, $value);
		}

		return $array;
	}
}

if (!function_exists("arr_divide"))
{
	function arr_divide($array)
	{
		return array(array_keys($array), array_values($array));
	}
}

if (!function_exists("arr_dot"))
{
	function arr_dot($array, $prepend = '')
	{
		$results = array();

		foreach ($array as $key => $value)
		{
			if (is_array($value))
			{
				$results = array_merge($results, arr_dot($value, $prepend.$key.'.'));
			}
			else
			{
				$results[$prepend.$key] = $value;
			}
		}

		return $results;
	}
}


if (!function_exists("arr_fromCSV"))
{
	function arr_except($array, $keys)
	{
		return array_diff_key($array, array_flip((array) $keys));
	}
}

if (!function_exists("arr_fetch"))
{
	function arr_fetch($array, $key)
	{
		foreach (explode('.', $key) as $segment)
		{
			$results = array();

			foreach ($array as $value)
			{
				if (array_key_exists($segment, $value = (array) $value))
				{
					$results[] = $value[$segment];
				}
			}

			$array = array_values($results);
		}

		return array_values($results);
	}
}

if (!function_exists("arr_flatten"))
{
	function arr_flatten($array)
	{
		$return = array();

		array_walk_recursive($array, function($x) use (&$return) { $return[] = $x; });

		return $return;
	}
}

if (!function_exists("arr_forget"))
{
	function arr_forget(&$array, $keys)
	{
		$original =& $array;

		foreach ((array) $keys as $key)
		{
			$parts = explode('.', $key);

			while (count($parts) > 1)
			{
				$part = array_shift($parts);

				if (isset($array[$part]) && is_array($array[$part]))
				{
					$array =& $array[$part];
				}
			}

			unset($array[array_shift($parts)]);

			// clean up after each pass
			$array =& $original;
		}
	}
}

if (!function_exists("arr_get"))
{
	function arr_get($array, $key, $default = null)
	{
		if (!arr_accessalike($array))
		{
			 if (is_null($key) || trim($key) == '')
			 {
	            return $array;
	        }

	        foreach (explode('.', $key) as $segment)
	        {
	            if (!is_object($array) || !isset($array->{$segment}))
	            {
	                return $default;
	            }

	            $array = $array->{$segment};
	        }

	        return $array;
		}
		else
		{
			if (is_null($key)) return $array;

			if (isset($array[$key])) return $array[$key];

			foreach (explode('.', $key) as $segment)
			{
				if ( ! is_array($array) || ! array_key_exists($segment, $array))
				{
					return $default;
				}

				$array = $array[$segment];
			}

			return $array;
		}
	}
}

if (!function_exists("arr_has"))
{
	function arr_has($array, $key)
	{
		if (empty($array) || is_null($key)) return false;

		if (array_key_exists($key, $array)) return true;

		foreach (explode('.', $key) as $segment)
		{
			if ( ! is_array($array) || ! array_key_exists($segment, $array))
			{
				return false;
			}

			$array = $array[$segment];
		}

		return true;
	}
}

if (!function_exists("arr_only"))
{
	function arr_only($array, $keys)
	{
		return array_intersect_key($array, array_flip((array) $keys));
	}
}

if (!function_exists("arr_alike"))
{
	function arr_alike($var)
	{
	  return is_array($var) ||
		($var instanceof ArrayAccess  &&
		$var instanceof Traversable  &&
		$var instanceof Serializable &&
		$var instanceof Countable);
	}
}

if (!function_exists("arr_accessalike"))
{
	function arr_accessalike($var)
	{
		return is_array($var) || $var instanceof ArrayAccess;
	}
}

if (!function_exists("arr_pluck"))
{
	function arr_pluck($array, $value, $key = null)
	{
		$results = array();

		foreach ($array as $item)
		{
			$itemValue = arr_get($item, $value);

			// If the key is "null", we will just append the value to the array and keep
			// looping. Otherwise we will key the array using the value of the key we
			// received from the developer. Then we'll return the final array form.
			if (is_null($key))
			{
				$results[] = $itemValue;
			}
			else
			{
				$itemKey = !arr_accessalike($item) ? $item->{$key} : $item[$key];

				$results[$itemKey] = $itemValue;
			}
		}

		return $results;
	}
}

if (!function_exists("arr_pull"))
{
	function arr_pull(&$array, $key, $default = null)
	{
		$value = arr_set($array, $key, $default);

		arr_forget($array, $key);

		return $value;
	}
}

if (!function_exists("arr_set"))
{
	function arr_set(&$array, $key, $value)
	{
		if (is_null($key)) return $array = $value;

		$keys = explode('.', $key);

		while (count($keys) > 1)
		{
			$key = array_shift($keys);

			// If the key doesn't exist at this depth, we will just create an empty array
			// to hold the next value, allowing us to create the arrays to hold final
			// values at the correct depth. Then we'll keep digging into the array.
			if ( ! isset($array[$key]) || ! is_array($array[$key]))
			{
				$array[$key] = array();
			}

			$array =& $array[$key];
		}

		$array[array_shift($keys)] = $value;

		return $array;
	}
}
